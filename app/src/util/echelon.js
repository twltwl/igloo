class Echelon {
  constructor(){
    this._list = [];
    console.log(window);
    window.addEventListener('storage', function(e){ 
      if(e.key === 'echelon'){
        let data = JSON.parse(e.newValue);
        this.onChange(data); 
      }
    }.bind(this), false);
  }
  
  register(callback){
    this._list.push(callback);
  }
  
  onChange(data){
    console.log('onChange');
    console.log(data);
    for (let k in this._list){
      let cb = this._list[k];
      cb(data);
    }
  }
  
  store(name, data){
    let strObj = JSON.stringify({'name':name, 'data':data});
    localStorage.setItem('echelon',strObj);
  }
}

export default new Echelon();