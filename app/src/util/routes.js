import Router from 'react-router';
import Start from '../components/pages/start';
import Test from '../components/pages/test';

export default [
  Router.createRoute({
    path: '/',
    name: 'start',
    handler: Start
  }),
  Router.createRoute({
    path: '/test',
    name: 'test',
    handler: Test
  })
];