class Swix{
  constructor(){
    this._env = null;
    let host = window.location.hash;
    if (host.match(/iglooadmin(3|300)\.dev2|igloo\.dev\.testarena/ig)){ //DEV2
      this._env = 'dev2';
    } else if (host.match(/igloo\.tc1\.testarena|igloo00\.tc1|igloo\.tc1/ig)){ //TC1
      this._env = 'tc1';
    } else if (host.match(/igloo\.tc2\.testarena|igloo00\.tc2|igloo\.tc2/ig)){ //TC2
      this._env = 'tc2';
    } else if (host.match(/igloo\.tc2\.testarena|igloo00\.tc2|igloo\.tc2/ig)){ //TC3
      this._env = 'tc3';
    } else if (host.match(/igloo\.lavinnetwork\.net|\.sw2$/ig)){ //LIVE
      this._env = 'sw2';
    } else { //LOCAL
      this._env = 'dev2';
    }
    
  }
  
  get env () { return this._env; } 
}

export default new Swix();