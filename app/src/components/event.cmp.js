import React from 'react';
import Emo from '../stores/emo/emo';

export default React.createClass({
  
  getInitialState () {
    return null;
  },
  
  componentWillMount () {

  },
  
  componentDidMount () {

  },
  
  componentWillUnmount () {

  },
  
  onChange () {

  },
  
  render () {
    let id = this.props.data;
    let event = Emo.getEvent(id);
    let markets = Emo.getMarkets(event.market_ids);
    return r('li',{},
      [
        r('span',{},event.id + ' ' + event.name),
        r('ul',{},
          markets.map((market) => r('li',{},
            [
              market.name,
              r('ul',{},
                market.outcome_ids.map(function(oid){
                  let outcome = Emo.getOutcome(oid);
                  return r('li',{},outcome.name);
                })
              )
            ]
          ))
        )
      ]
    );
  }
});