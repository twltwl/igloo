import React from 'react';
import Header from '../main/header.cmp';
import Menu from '../main/menu.cmp';
import Main from '../main.cmp';

export default React.createClass({
  render() {
    return r('div',{},
      [
        r(Header),
        r(Menu),
        r(Main)
      ]
    );
  }
});