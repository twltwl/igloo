import React from 'react';
import Emo from '../stores/emo/emo';
import Actions from '../actions/main-actions';
import EventComp from './event.cmp';

let getEvents = () => {
  return {events : Emo.getEvents()}
}

export default React.createClass({
  
  getInitialState () {
    return getEvents();
  },
  
  componentWillMount () {
    Emo.addListener('change', this.onChange);
    Actions.getEvents();
  },
  
  componentDidMount () {
    Emo.addListener('change', this.onChange);
  },
  
  componentWillUnmount () {
    Emo.removeListener('change', this.onChange);
  },
  
  onChange () {
    this.setState(getEvents());
  },
  
  render () {
    let events = this.state.events;
    return r('div',{className:'container'},
      r('ul',{},
        //events.map((event) => r('li',{},event.id + ' ' + event.name) )
        events.map((event) => r(EventComp, {data:event.id}) )
      )
    );
  }
});