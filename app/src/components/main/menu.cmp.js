import React from 'react';

export default React.createClass({
  render() {
    return r('ul',{className:'menu'},
      [
        r('li',{},
          r('a',{href:'#/'},'main')
        ),
        r('li',{},
          r('a',{href:'#/test'},'test')
        )
      ]
    );
  }
});