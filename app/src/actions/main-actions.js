import Dispatcher from '../dispatcher';
import constants from '../constants';

let mainActions = {
  getEvents() {
    Dispatcher.dispatch({
      actionType: constants.actions.ADD_EVENT,
      data: {
        id:1,
        name:'blubb',
        markets:[
          {
            id:1,name:'apa',event_id:1, outcomes:[{id:1,name:'o1'},{id:2,name:'o2'}]},
            {id:2,name:'trollolol',event_id:1, outcomes:[{id:3,name:'a1'},{id:4,name:'a2'}]}
        ]
      }
    });
  }

};

export {mainActions as default};
