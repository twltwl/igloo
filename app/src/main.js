import React from 'react';
import Router from 'react-router';
import routes from './util/routes';
import Swix from './util/swix';
import Echelon from './util/echelon';

window.r = React.createElement;

if (document.readyState === 'complete' || document.readyState === 'loaded') {
  init();
} else {
  document.addEventListener('DOMContentLoaded', init);
}

let apa = function(){ 
  this.foo = 'apa';
}

function init() {
  document.title = 'Igloo - ' + Swix.env
  document.removeEventListener('DOMContentLoaded', init);
  let a = new apa();
  Echelon.register(function(){ console.log(this.foo); }.bind(a));
  setInterval(function(){ Echelon.store('test',Math.random()); }, 2000)
  
  Router.run(routes, Router.HashLocation, (Page) => {
    React.render(r(Page), document.getElementById('app'));
  });
}
