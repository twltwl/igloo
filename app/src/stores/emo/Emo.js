﻿import Dispatcher from '../../dispatcher';
import constants from '../../constants';
import EventEmitter from 'events';
import R from 'ramda';
import Immutable from 'immutable';
import Event from './event_record';
import Market from './market_record';
import Outcome from './outcome_record';
import Actions from '../../actions/main-actions';

let _events   = Immutable.Map();
let _markets  = Immutable.Map();
let _outcomes = Immutable.Map();

let Emo = R.merge(EventEmitter.prototype, {

  getEvents () {
    return _events.toArray();
  },
  
  getEvent (id) {
    return _events.get(id);
  },
  
  getMarkets (ids){
    let m = [];
    for(let k in ids){
      m.push(this.getMarket(ids[k]));
    }
    return m;
  },
  
  getMarket (id) {
    return _markets.get(id);
  },
  
  getOutcomes(ids){
    let o = [];
    for(let k in ids){
      o.push(this.getOutcome(ids[k]));
    }
    return o;
  },
  
  getOutcome(id){
    return _outcomes.get(id);
  },
  
  dispatcherIndex: Dispatcher.register(action => {
    switch (action.actionType) {
      case constants.actions.ADD_EVENT:
        let id = action.data.id;
        _events = _events.set(action.data.id,
          new Event({
            id         : action.data.id,
            name       : action.data.name,
            market_ids : action.data.markets.map(function(m){ return m.id })
          })
        );
        
        for (let k in action.data.markets){
          let market = action.data.markets[k];
          _markets = _markets.set(market.id,
            new Market({
              id          : market.id,
              name        : market.name,
              event_id    : market.event_id,
              outcome_ids : market.outcomes.map(function(m){ return m.id })
            })
          );
          for (let o in market.outcomes){
            let outcome = market.outcomes[o];
            _outcomes = _outcomes.set(outcome.id,
              new Outcome({
                id   : outcome.id,
                name : outcome.name
              })
            )
          }
        }
        
        Emo.emit('change');
      break;
    }
    return true;
  })

});


export {Emo as default};
