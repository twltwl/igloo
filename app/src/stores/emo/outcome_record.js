﻿import Immutable from 'immutable';

let Outcome = Immutable.Record({
  id       : undefined,
  name     : undefined
});

export default Outcome;