import Immutable from 'immutable';

let Market = Immutable.Record({
  id          : undefined,
  event_id    : undefined,
  name        : undefined,
  outcome_ids : undefined
});

export default Market;