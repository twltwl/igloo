import Immutable from 'immutable';

let Event = Immutable.Record({
  id         : undefined,
  name       : undefined,
  market_ids : undefined
});

export default Event;